//
//  main.swift
//  AoC2019Task03
//
//  Created by Grzegorz Lieske on 01/12/2019.
//  Copyright © 2019 Grzegorz Lieske. All rights reserved.
//

import Foundation

public class Point : Hashable {
    public static func == (lhs: Point, rhs: Point) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
    
    var hsh: String { return "\(self.point.x)_\(self.point.y)" }
    var point: CGPoint
    
    init(_ point: CGPoint) {
        self.point = point
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(self.hsh)
    }
}

let fm = FileManager()

public func readInputFile() -> [String] {
    let inputFile = URL(fileURLWithPath: "./input.txt")
    print("Path: \(inputFile.path)")
    do {
        let data = try String(contentsOfFile: inputFile.path, encoding: .utf8)
        return data.components(separatedBy: .newlines).filter { $0 != "" }
        
    } catch {
        print(error)
    }
    return [];
}

public func mapToCGPointsBF(wire: String) -> [Point] {
    let w1 = wire.components(separatedBy: ",")
    var wirePts: [Point] = []
    var x = 0
    var y = 0
    for elem: String in w1 {
        switch (String(elem.first!)) {
        case "R":
            for _ in 1...Int(String(elem.dropFirst()))! {
                wirePts.append(Point(CGPoint(x: x+1, y: y)))
                x += 1
            }
            break
        case "U":
            for _ in 1...Int(String(elem.dropFirst()))! {
                wirePts.append(Point(CGPoint(x: x, y: y+1)))
                y += 1
            }
            break
        case "L":
            for _ in 1...Int(String(elem.dropFirst()))! {
                wirePts.append(Point(CGPoint(x: x-1, y: y)))
                x -= 1
            }
            break
        case "D":
            for _ in 1...Int(String(elem.dropFirst()))! {
                wirePts.append(Point(CGPoint(x: x, y: y-1)))
                y -= 1
            }
            break
        default:
            print("error")
        }
    }
    return wirePts
}

public func CalculateManhattanDistance(p1: CGPoint, p2: CGPoint) -> CGFloat {
    return abs(p1.x - p2.x) + abs(p1.y - p2.y)
}

public func calculateBF(wire1: String, wire2: String) -> (Int, Int) {
    let w1 = mapToCGPointsBF(wire: wire1)
    let w2 = mapToCGPointsBF(wire: wire2)
    let central = CGPoint(x: 0, y: 0)
    var closestIntersection: CGFloat?
    var closestDistance: (Int?, Int?) = (nil, nil)
    
    let x = Array(Set(w1).intersection(Set(w2)))
    
    for intersection in x {
        let w1i = w1.firstIndex(of: intersection)
        let w2i = w2.firstIndex(of: intersection)
        let manhattan = CalculateManhattanDistance(p1: central, p2: intersection.point)
        if (closestIntersection == nil) {
            closestIntersection = manhattan
            closestDistance.0 = w1i
            closestDistance.1 = w2i
        } else {
            let cI = closestIntersection!
            if (!(cI.isLessThanOrEqualTo(manhattan))) {
                closestIntersection = manhattan
            }
            if (closestDistance.0! + closestDistance.1! + 2 > w1i! + w2i!) { // + 2 because arrays are indexed from 0
                closestDistance.0 = w1i! + 1
                closestDistance.1 = w2i! + 1
            }
        }
    }
    return (Int(closestIntersection!), closestDistance.0! + closestDistance.1!)
}

print("Hello, It's Advent of Code 2019 Task 03")
let startTS = Date().timeIntervalSince1970

let data: [String] = readInputFile();
let result: (Int, Int) = calculateBF(wire1: data[0], wire2: data[1])

let finishTS = Date().timeIntervalSince1970

print("Part1: \(result.0)")
print("Part2: \(result.1)")
print("Time: \(finishTS - startTS) seconds")


