//
//  main.swift
//  AoC2019Task06
//
//  Created by Grzegorz Lieske on 01/12/2019.
//  Copyright © 2019 Grzegorz Lieske. All rights reserved.
//

import Foundation

public class Node<T> {
    var data: T
    var children: [Node<T>] = []
    var parent: Node<T>?
    var level: Int
    
    init(data: T, parent: Node<T>?) {
        self.data = data
        self.parent = parent
        self.level = parent == nil ? 0 : parent!.level + 1
    }
    
    func addNode(child: Node<T>) {
        child.level = self.level + 1
        children.append(child)
    }
    
    func search(element: T) -> Node<T>? {
        if "\(element)" == "\(self.data)" {
            return self
        } else {
            for child in children {
                if let result = child.search(element: element) {
                    return result
                }
            }
            return nil
        }
    }
    
    func calculateOrbits() -> Int {
        var orbits = 0
        for child in children {
            orbits += child.level
            orbits += child.calculateOrbits()
        }
        return orbits
    }
}

public func readInputFile() -> [String] {
    let inputFile = URL(fileURLWithPath: "./input.txt")
    print("Path: \(inputFile.path)")
    do {
        let data = try String(contentsOfFile: inputFile.path, encoding: .utf8)
        return data.components(separatedBy: .newlines).filter { $0 != "" }
    } catch {
        print(error)
    }
    return [];
}

typealias Input = (from: String, to: String)

func mapToTree(parent: Node<String>, children: [Input], _ dict: Dictionary<String, [Input]>) {
    for child in children {
        let node = Node<String>(data: child.to, parent: parent)
        let chi = dict[child.to] ?? []
        mapToTree(parent: node, children: chi, dict)
        parent.addNode(child: node)
    }
}

public func howLongToSantasOrbit(me: Node<String>, santa: Node<String>) -> Int {
    var distance = 0
    var p1 = me.level > santa.level ? me : santa
    var p2 = me.level > santa.level ? santa : me
    let destinationLevel = p2.level
    var currentLevel = p1.level
    while currentLevel > destinationLevel { // move the deeper one to the same level like closer to root
        distance += 1
        p1 = p1.parent!
        currentLevel -= 1
    }
    while p1.parent!.data != p2.parent!.data { // both go up until common grand parent would be found
        distance += 2
        p1 = p1.parent!
        p2 = p2.parent!
    }
    return distance
}

public func calc(data: [String]) {
    let split = data
        .map{ $0.components(separatedBy: ")") }
        .map { return Input(from: $0[0], to: $0[1]) }
    let dict = Dictionary(grouping: split) { $0.from }
    let children = dict["COM"]!
    let rootNode = Node<String>(data: "COM", parent: nil)
    mapToTree(parent: rootNode, children: children, dict)
    print("Part1: orbits: \(rootNode.calculateOrbits())")
    let me = rootNode.search(element: "YOU")!
    let santa = rootNode.search(element: "SAN")!
    print("Part2: distance to Santa's orbit: \(howLongToSantasOrbit(me: me, santa: santa))")
}

print("Hello, It's Advent of Code 2019 Task 06")
let startTS = Date().timeIntervalSince1970

let data: [String] = readInputFile()
calc(data: data)

let finishTS = Date().timeIntervalSince1970

print("Time: \(finishTS - startTS) seconds")
