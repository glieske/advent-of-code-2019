//
//  main.swift
//  AoC2019Task05
//
//  Created by Grzegorz Lieske on 01/12/2019.
//  Copyright © 2019 Grzegorz Lieske. All rights reserved.
//

import Foundation

extension StringProtocol {
    subscript(characterIndex: Int) -> String {
        return String(self[index(startIndex, offsetBy: characterIndex)])
    }
}

public class IntComputer {
    
    private class Position {
        public var mode: Mode
        public var index: Int64
        init(mode: Mode, index: Int64) {
            self.mode = mode
            self.index = index
        }
    }
    
    private class Config {
        public var pos1: Position
        public var pos2: Position
        public var pos3: Position
        public var opCode: Int64
        init(_ opcode: Int64, _ poss: [Position]) {
            self.opCode = opcode
            self.pos1 = poss[0]
            self.pos2 = poss[1]
            self.pos3 = poss[2]
        }
    }
    
    private enum Mode {
        case positional
        case immediate
        case relative
    }
    
    private var memory: [Int64]
    private var extendedMemory: Dictionary<Int,Int64>
    private var i : Int = 0
    private var halted: Bool = false
    private var lastOutputPosition: Position
    private var phaseSet: Bool = false
    private var relativeBase: Int64 = 0
    
    init(memory: [Int64]) {
        self.memory = memory
        self.extendedMemory = Dictionary<Int, Int64>()
        lastOutputPosition = Position(mode: .immediate, index: 0)
    }
    
    public func isHalted() -> Bool {
        return halted
    }
    
    public func getCurrentValue() -> Int64 {
        return getValue(lastOutputPosition)
    }
    
    private func getConfig(code: Int64) -> Config {
        var str = String(code)
        while str.count < 5 {
            str = "0\(str)"
        }
        return Config(
            code,
            str.dropLast().dropLast()
                .reversed()
                .enumerated()
                .map{
                    (index, elem) in
                    let el = Int64(String(elem))
                    var mode: Mode = .positional
                    switch (el) {
                    case 0:
                        mode = .positional
                        break
                    case 1:
                        mode = .immediate
                        break
                    case 2:
                        mode = .relative
                        break
                    
                    default:
                    print("ERROR! invalid mode")
                    }
                    return Position(mode: mode, index: Int64(i) + Int64(index) + 1)
            }
        )
    }
    
    public func execProgram(phase: Int64, instruct: Int64) {
        if (halted) {
            print("Computer is halted")
            return
        }
        var instruction = memory[i]
        var opCode = instruction % 100
        while opCode != 99 {
            let conf = getConfig(code: instruction)
            switch (opCode) {
            case 1: // add
                let pos1 = getValue(conf.pos1)
                let pos2 = getValue(conf.pos2)
                setValue(conf.pos3, value: pos1 + pos2)
                i += 4
                break
            case 2: // multiply
                let pos1 = getValue(conf.pos1)
                let pos2 = getValue(conf.pos2)
                setValue(conf.pos3, value: pos1 * pos2)
                i += 4
                break
            case 3: // get input
                if (phaseSet == false) {
                    setValue(conf.pos1, value: phase)
                    phaseSet = true
                } else {
                    setValue(conf.pos1, value: instruct)
                }
                print("INPUT (AUTO): \(getValue(conf.pos1))")
                i += 2
                break
            case 4: // output
                lastOutputPosition = conf.pos1
                i += 2
                return
            case 5: //jump true
                let value = getValue(conf.pos1)
                let pos = getValue(conf.pos2)
                if (value != 0) {
                    i = Int(pos)
                    //do not increment i
                } else {
                    i += 3
                }
                break
            case 6: //jump false
                let value = getValue(conf.pos1)
                let pos = getValue(conf.pos2)
                if (value == 0) {
                    i = Int(pos)
                    //do not increment i
                } else {
                    i += 3
                }
                break
            case 7: //less
                let pos1 = getValue(conf.pos1)
                let pos2 = getValue(conf.pos2)
                let pos3 = conf.pos3
                let value = pos1 < pos2 ? 1 : 0
                setValue(pos3, value: Int64(value))
                i += 4
                break
            case 8: //eq
                let pos1 = getValue(conf.pos1)
                let pos2 = getValue(conf.pos2)
                let pos3 = conf.pos3
                let value = pos1 == pos2 ? 1 : 0
                setValue(pos3, value: Int64(value))
                i += 4
                break
            case 9: //change relative base
                let value = getValue(conf.pos1)
                relativeBase += value
                print("Changed Relative base to: \(relativeBase)")
                i += 2
            default:
                print ("error - received OpCode \(opCode)")
            }
            instruction = memory[i]
            opCode = instruction % 100
        }
        halted = true
    }
    
    private func getAddress(_ pos: Position) -> Int {
        let addr: Int
        if (pos.mode == .immediate) {
            addr = Int(pos.index)
        } else if (pos.mode == .positional) {
            addr = Int(getValue(Position(mode: .immediate, index: pos.index)))
        } else {
            let sub = getValue(Position(mode: .immediate, index: pos.index))
            addr = Int(sub + relativeBase)
        }
        return addr
    }
    
    private func getValue(_ pos: Position) -> Int64 {
        let addr: Int = getAddress(pos)
        if (addr > memory.count - 1) {
            return extendedMemory[addr] ?? 0
        } else {
            return memory[addr]
        }
    }
    
    private func setValue(_ pos: Position, value: Int64) {
        let addr = getAddress(pos)
        if (addr > memory.count - 1) {
            extendedMemory[addr] = value
        } else {
            memory[addr] = value
        }
    }
}

public func readInputFile() -> [String] {
    let inputFile = URL(fileURLWithPath: "./input.txt")
    print("Path: \(inputFile.path)")
    do {
        let data = try String(contentsOfFile: inputFile.path, encoding: .utf8)
        return data.components(separatedBy: .newlines).filter { $0 != "" }
    } catch {
        print(error)
    }
    return [];
}

public func calcPart1(data: [Int64]) -> Int64 {
    let computer = IntComputer(memory: data)
    var output: Int64 = 0
    repeat {
        computer.execProgram(phase: 1, instruct: 1)
        output = computer.getCurrentValue()
        print(output)
    } while (computer.isHalted() == false)
    return output
}

public func calcPart2(data: [Int64]) -> Int64 {
    let computer = IntComputer(memory: data)
    var output: Int64 = 0
    repeat {
        computer.execProgram(phase: 2, instruct: 2)
        output = computer.getCurrentValue()
        print(output)
    } while (computer.isHalted() == false)
    return output
}

public func calc(data: String) -> Void {
    let input: [Int64] = data.components(separatedBy: ",").map { Int64($0)! }
    let part1 = calcPart1(data: input)
    let part2 = calcPart2(data: input)
    print("Part1: BOOST value: \(part1)")
    print("Part2: coordinates: \(part2)")
}

print("Hello, It's Advent of Code 2019 Task 09")
let startTS = Date().timeIntervalSince1970

let data: [String] = readInputFile()
calc(data: data.first!)

let finishTS = Date().timeIntervalSince1970

print("Time: \(finishTS - startTS) seconds")
