//
//  main.swift
//  AoC2019Task13
//
//  Created by Grzegorz Lieske on 01/12/2019.
//  Copyright © 2019 Grzegorz Lieske. All rights reserved.
//

import Foundation

extension StringProtocol {
    subscript(characterIndex: Int) -> String {
        return String(self[index(startIndex, offsetBy: characterIndex)])
    }
}

public class IntComputer {
    
    private class Position {
        public var mode: Mode
        public var index: Int64
        init(mode: Mode, index: Int64) {
            self.mode = mode
            self.index = index
        }
    }
    
    private class Config {
        public var pos1: Position
        public var pos2: Position
        public var pos3: Position
        public var opCode: Int64
        init(_ opcode: Int64, _ poss: [Position]) {
            self.opCode = opcode
            self.pos1 = poss[0]
            self.pos2 = poss[1]
            self.pos3 = poss[2]
        }
    }
    
    private enum Mode {
        case positional
        case immediate
        case relative
    }
    
    private var memory: [Int64]
    private var extendedMemory: Dictionary<Int,Int64>
    private var i : Int = 0
    private var halted: Bool = false
    private var lastOutputPosition: Position
    private var phaseSet: Bool = false
    private var relativeBase: Int64 = 0
    
    init(memory: [Int64]) {
        self.memory = memory
        self.extendedMemory = Dictionary<Int, Int64>()
        lastOutputPosition = Position(mode: .immediate, index: 0)
    }
    
    public func isHalted() -> Bool {
        return halted
    }
    
    public func getCurrentValue() -> Int64 {
        return getValue(lastOutputPosition)
    }
    
    private func getConfig(code: Int64) -> Config {
        var str = String(code)
        while str.count < 5 {
            str = "0\(str)"
        }
        return Config(
            code,
            str.dropLast().dropLast()
                .reversed()
                .enumerated()
                .map{
                    (index, elem) in
                    let el = Int64(String(elem))
                    var mode: Mode = .positional
                    switch (el) {
                    case 0:
                        mode = .positional
                        break
                    case 1:
                        mode = .immediate
                        break
                    case 2:
                        mode = .relative
                        break
                    
                    default:
                    print("ERROR! invalid mode")
                    }
                    return Position(mode: mode, index: Int64(i) + Int64(index) + 1)
            }
        )
    }
    
    public func execProgram(phase: Int64, instruct: Int64) {
        if (halted) {
            print("Computer is halted")
            return
        }
        var instruction = memory[i]
        var opCode = instruction % 100
        while opCode != 99 {
            let conf = getConfig(code: instruction)
            switch (opCode) {
            case 1: // add
                let pos1 = getValue(conf.pos1)
                let pos2 = getValue(conf.pos2)
                setValue(conf.pos3, value: pos1 + pos2)
                i += 4
                break
            case 2: // multiply
                let pos1 = getValue(conf.pos1)
                let pos2 = getValue(conf.pos2)
                setValue(conf.pos3, value: pos1 * pos2)
                i += 4
                break
            case 3: // get input
                if (phaseSet == false) {
                    setValue(conf.pos1, value: phase)
                    phaseSet = true
                } else {
                    setValue(conf.pos1, value: instruct)
                }
                //print("INPUT (AUTO): \(getValue(conf.pos1))")
                i += 2
                break
            case 4: // output
                lastOutputPosition = conf.pos1
                i += 2
                return
            case 5: //jump true
                let value = getValue(conf.pos1)
                let pos = getValue(conf.pos2)
                if (value != 0) {
                    i = Int(pos)
                    //do not increment i
                } else {
                    i += 3
                }
                break
            case 6: //jump false
                let value = getValue(conf.pos1)
                let pos = getValue(conf.pos2)
                if (value == 0) {
                    i = Int(pos)
                    //do not increment i
                } else {
                    i += 3
                }
                break
            case 7: //less
                let pos1 = getValue(conf.pos1)
                let pos2 = getValue(conf.pos2)
                let pos3 = conf.pos3
                let value = pos1 < pos2 ? 1 : 0
                setValue(pos3, value: Int64(value))
                i += 4
                break
            case 8: //eq
                let pos1 = getValue(conf.pos1)
                let pos2 = getValue(conf.pos2)
                let pos3 = conf.pos3
                let value = pos1 == pos2 ? 1 : 0
                setValue(pos3, value: Int64(value))
                i += 4
                break
            case 9: //change relative base
                let value = getValue(conf.pos1)
                relativeBase += value
                //print("Changed Relative base to: \(relativeBase)")
                i += 2
            default:
                print ("error - received OpCode \(opCode)")
            }
            instruction = memory[i]
            opCode = instruction % 100
        }
        halted = true
    }
    
    private func getAddress(_ pos: Position) -> Int {
        let addr: Int
        if (pos.mode == .immediate) {
            addr = Int(pos.index)
        } else if (pos.mode == .positional) {
            addr = Int(getValue(Position(mode: .immediate, index: pos.index)))
        } else {
            let sub = getValue(Position(mode: .immediate, index: pos.index))
            addr = Int(sub + relativeBase)
        }
        return addr
    }
    
    private func getValue(_ pos: Position) -> Int64 {
        let addr: Int = getAddress(pos)
        if (addr > memory.count - 1) {
            return extendedMemory[addr] ?? 0
        } else {
            return memory[addr]
        }
    }
    
    private func setValue(_ pos: Position, value: Int64) {
        let addr = getAddress(pos)
        if (addr > memory.count - 1) {
            extendedMemory[addr] = value
        } else {
            memory[addr] = value
        }
    }
    
    public func injectValue(position: Int, value: Int64) -> Void {
        self.memory[position] = value
    }
    
    public func resetInstructionPointer() -> Void {
        self.i = 0
        self.halted = false
    }
}

public class TileFactory {
    public static func build(position: CGPoint, type: TileType) -> Tile {
        switch (type) {
        case .empty:
            return EmptyTile(position: position)
        case .ball:
            return BallTile(position: position)
        case .block:
            return BlockTile(position: position)
        case .horizontalPaddle:
            return HorizontalPaddleTile(position: position)
        case .wall:
            return WallTile(position: position)
        }
    }
}

public enum TileType : Int64 {
    case empty = 0
    case wall = 1
    case block = 2
    case horizontalPaddle = 3
    case ball = 4
}

public protocol Tile {
    var position: CGPoint { get set }
    var objectType: TileType { get }
    var renderable: Bool { get }
    var destructable: Bool { get }
}

public class EmptyTile : Tile {
    public var renderable: Bool = false
    public var destructable: Bool = false
    public var position: CGPoint
    public var objectType: TileType
    init(position: CGPoint) {
        self.position = position
        self.objectType = .empty
    }
}

public class WallTile : Tile {
    public var renderable: Bool = true
    public var destructable: Bool = false
    public var position: CGPoint
    public var objectType: TileType
    init(position: CGPoint) {
        self.position = position
        self.objectType = .wall
    }
}

public class BlockTile : Tile {
    public var renderable: Bool = true
    public var destructable: Bool = true
    public var position: CGPoint
    public var objectType: TileType
    init(position: CGPoint) {
        self.position = position
        self.objectType = .block
    }
}

public class HorizontalPaddleTile : Tile {
    public var renderable: Bool = true
    public var destructable: Bool = false
    public var position: CGPoint
    public var objectType: TileType
    init(position: CGPoint) {
        self.position = position
        self.objectType = .horizontalPaddle
    }
}

public class BallTile : Tile {
    public var renderable: Bool = true
    public var destructable: Bool = false
    public var position: CGPoint
    public var objectType: TileType
    init(position: CGPoint) {
        self.position = position
        self.objectType = .ball
    }
}

public class Display {
    var objects: [Tile] = []
}

public class Joystick {
    public enum Position: Int64 {
        case neutral = 0
        case left = -1
        case right = 1
    }
    var position: Joystick.Position
    init(position: Joystick.Position) {
        self.position = position
    }
}

public class Game {
    let computer: IntComputer
    let display: Display
    var score: Int64 = 0
    let joystick: Joystick
    var ball: BallTile?
    var paddle: HorizontalPaddleTile?
    private var minX: Int?
    private var minY: Int?
    private var maxX: Int?
    private var maxY: Int?
    
    init (data: [Int64]) {
        self.computer = IntComputer(memory: data)
        self.display = Display()
        self.joystick = Joystick(position: .right)
    }
    
    private func getBall() -> BallTile {
        return display.objects.first { $0.objectType == .ball } as! BallTile
    }
    
    private func getHorizontalPaddle() -> HorizontalPaddleTile {
        return display.objects.first { $0.objectType == .horizontalPaddle } as! HorizontalPaddleTile
    }
    
    private func getInput(instruction: Int64 = 1) -> (Int64, Int64, Int64) {
        computer.execProgram(phase: instruction, instruct: instruction)
        let x = computer.getCurrentValue()
        computer.execProgram(phase: instruction, instruct: instruction)
        let y = computer.getCurrentValue()
        computer.execProgram(phase: instruction, instruct: instruction)
        let type = computer.getCurrentValue()
        return (x, y, type)
    }
    
    public func draw() -> Void {
        print("Score: \(score)")
        let objs = display.objects
        for y in minY!...maxY! {
            for x in minX!...maxX! {
                if let point = objs.first(where: { Int($0.position.x) == x && Int($0.position.y) == y }) {
                    var ch = " "
                    switch (point.objectType) {
                    case .ball:
                        ch = "@"
                        break
                    case .block:
                        ch = "*"
                        break
                    case .horizontalPaddle:
                        ch = "="
                        break
                    case .wall:
                        ch = "H"
                        break
                    default:
                        break
                    }
                    print(ch, terminator: "")
                } else {
                    print(" ", terminator: "")
                }
            }
            print("")
        }
    }
    public func run() {
        while computer.isHalted() == false {
            var (x, y, type) = getInput()
            let object = TileFactory.build(
                position: CGPoint(x: Int(x), y: Int(y)),
                type: TileType(rawValue: type)!
            )
            display.objects.append(object)
        }
    }
    
    private func prefetchDrawDetails() -> Void {
        minX = Int(display.objects.map{ $0.position.x }.min()!)
        maxX = Int(display.objects.map{ $0.position.x }.max()!)
        minY = Int(display.objects.map{ $0.position.y }.min()!)
        maxY = Int(display.objects.map{ $0.position.y }.max()!)
    }
    
    public func play() {
        computer.resetInstructionPointer()
        computer.injectValue(position: 0, value: 2)
        display.objects = display.objects.filter { $0.objectType != .empty }
        prefetchDrawDetails()
        ball = getBall()
        paddle = getHorizontalPaddle()
        while computer.isHalted() == false {
            joystick.position = getJoystickNextPosition(ball: ball!, paddle: paddle!)
            let (x, y, type) = getInput(instruction: joystick.position.rawValue)
            if (x == -1 && y == 0) {
                score = type
                print("Score: \(score)")
            } else {
                if (type == TileType.horizontalPaddle.rawValue) {
                    paddle!.position.x = CGFloat(x)
                    paddle!.position.y = CGFloat(y)
                } else if (type == TileType.ball.rawValue) {
                    ball!.position.x = CGFloat(x)
                    ball!.position.y = CGFloat(y)
                    //draw()
                }
            }
        }
    }
    
    private func getJoystickNextPosition(ball: BallTile, paddle: HorizontalPaddleTile) -> Joystick.Position {
        if paddle.position.x > ball.position.x {
            return .left
        } else if paddle.position.x == ball.position.x {
            return .neutral
        } else {
            return .right
        }
    }
    
    public func getDisplay() -> Display {
        return display
    }
}

public func readInputFile() -> [String] {
    let inputFile = URL(fileURLWithPath: "./input.txt")
    print("Path: \(inputFile.path)")
    do {
        let data = try String(contentsOfFile: inputFile.path, encoding: .utf8)
        return data.components(separatedBy: .newlines).filter { $0 != "" }
    } catch {
        print(error)
    }
    return [];
}

public func calcPart1(data: [Int64]) -> Int {
    let game = Game(data: data)
    game.run()
    let display = game.getDisplay()
    return display.objects.filter { $0.objectType == .block }.count
}

public func calcPart2(data: [Int64]) -> Int64 {
    let game = Game(data: data)
    game.run()
    game.play()
    return game.score
}

public func calc(data: String) -> Void {
    let input: [Int64] = data.components(separatedBy: ",").map { Int64($0)! }
    let part1 = calcPart1(data: input)
    let part2 = calcPart2(data: input)
    print("Part1: number of blocks: \(part1)")
    print("Part2: score: \(part2)")
}

print("Hello, It's Advent of Code 2019 Task 13")
let startTS = Date().timeIntervalSince1970

let data: [String] = readInputFile()
calc(data: data.first!)

let finishTS = Date().timeIntervalSince1970

print("Time: \(finishTS - startTS) seconds")
