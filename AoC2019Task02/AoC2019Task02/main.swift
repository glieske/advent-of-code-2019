//
//  main.swift
//  AoC2019Task02
//
//  Created by Grzegorz Lieske on 01/12/2019.
//  Copyright © 2019 Grzegorz Lieske. All rights reserved.
//

import Foundation

let fm = FileManager()

public func getInputFilePath() -> URL {
    while (true) {
        print("Provide path of input file: ")
        if let path = readLine() {
            let url = URL(fileURLWithPath: path)
            if (fm.fileExists(atPath: url.path)) {
                return url
            } else {
                print("File not found")
            }
        }
    }
}

public func readInputFile(inputFile: URL) -> [String] {
    print("Path: \(inputFile.path)")
    do {
        let data = try String(contentsOfFile: inputFile.path, encoding: .utf8)
        return data.components(separatedBy: .newlines).filter { $0 != "" }
    } catch {
        print(error)
    }
    return [];
}

public func execProgram(data: [Int], rep1: Int = 12, rep2: Int = 2) -> Int {
    var input = data;
    input[1] = rep1
    input[2] = rep2
    var i = 0
    while i < input.count {
        let opCode = input[i]
        if (opCode == 99) {
            break;
        }
        let pos1 = input[i+1]
        let pos2 = input[i+2]
        let ouPos = input[i+3]
        input[ouPos] = opCode == 1 ? input[pos1] + input[pos2] : input[pos1] * input[pos2]
        i += 4
    }
    return input[0]
}

public func calcPart1(data: String) -> Int {
    let input: [Int] = data.components(separatedBy: ",").map {Int($0)!}
    return execProgram(data: input)
}

public func calcPart2(data: String) -> Int {
    let expected = 19690720
    let input: [Int] = data.components(separatedBy: ",").map {Int($0)!}
    for noun in 0...99 {
        for verb in 0...99 {
            let val = execProgram(data: input, rep1: noun, rep2: verb)
            if (val == expected) {
                return 100 * noun + verb
            }
        }
    }
    return -1
}

print("Hello, It's Advent of Code 2019 Task 02")
let inputFile: URL = getInputFilePath()
let data: [String] = readInputFile(inputFile: inputFile);
let result: Int = calcPart1(data: data.first!)
let result2: Int = calcPart2(data: data.first!)

print("Result: \(result)")
print("Result2: \(result2)")


