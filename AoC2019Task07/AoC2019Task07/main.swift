//
//  main.swift
//  AoC2019Task05
//
//  Created by Grzegorz Lieske on 01/12/2019.
//  Copyright © 2019 Grzegorz Lieske. All rights reserved.
//

import Foundation

extension StringProtocol {
    subscript(characterIndex: Int) -> String {
        return String(self[index(startIndex, offsetBy: characterIndex)])
    }
}

public class IntComputer {
    
    private class Position {
        public var mode: Mode
        public var index: Int
        init(mode: Mode, index: Int) {
            self.mode = mode
            self.index = index
        }
    }
    
    private class Config {
        public var pos1: Position
        public var pos2: Position
        public var pos3: Position
        public var opCode: Int
        init(_ opcode: Int, _ poss: [Position]) {
            self.opCode = opcode
            self.pos1 = poss[0]
            self.pos2 = poss[1]
            self.pos3 = poss[2]
        }
    }
    
    private enum Mode {
        case positional
        case immediate
    }
    
    private var memory: [Int]
    private var i : Int = 0
    private var halted: Bool = false
    private var lastOutputPosition: Position
    private var phaseSet: Bool = false
    
    init(memory: [Int]) {
        self.memory = memory
        lastOutputPosition = Position(mode: .immediate, index: 0)
    }
    
    public func isHalted() -> Bool {
        return halted
    }
    
    public func getCurrentValue() -> Int {
        return getValue(lastOutputPosition)
    }
    
    private func getConfig(code: Int) -> Config {
        var str = String(code)
        while str.count < 5 {
            str = "0\(str)"
        }
        return Config(
            code,
            str.dropLast().dropLast()
                .reversed()
                .enumerated()
                .map{ (index, elem) in return Position(mode: (Int(String(elem)) == 0 ? .positional : .immediate), index: i + index + 1) }
        )
    }
    
    public func execProgram(phase: Int, instruct: Int) {
        if (halted) {
            print("Computer is halted")
            return
        }
        var instruction = memory[i]
        var opCode = instruction % 100
        while opCode != 99 {
            let conf = getConfig(code: instruction)
            switch (opCode) {
            case 1: // add
                let pos1 = getValue(conf.pos1)
                let pos2 = getValue(conf.pos2)
                let pos3 = Position(mode: .immediate, index: memory[i+3])
                memory[pos3.index] = pos1 + pos2
                i += 4
                break
            case 2: // multiply
                let pos1 = getValue(conf.pos1)
                let pos2 = getValue(conf.pos2)
                let pos3 = Position(mode: .immediate, index: memory[i+3])
                memory[pos3.index] = pos1 * pos2
                i += 4
                break
            case 3: // get input
                let pos = Position(mode: .immediate, index: memory[i+1])
                if (phaseSet == false) {
                    memory[pos.index] = phase
                    phaseSet = true
                } else {
                    memory[pos.index] = instruct
                }
                i += 2
                break
            case 4: // output
                lastOutputPosition = conf.pos1
                i += 2
                return
            case 5: //jump true
                let value = getValue(conf.pos1)
                let pos = Position(mode: .immediate, index: getValue(conf.pos2))
                if (value != 0) {
                    i = pos.index
                    //do not increment i
                } else {
                    i += 3
                }
                break
            case 6: //jump false
                let value = getValue(conf.pos1)
                let pos = Position(mode: .immediate, index: getValue(conf.pos2))
                if (value == 0) {
                    i = pos.index
                    //do not increment i
                } else {
                    i += 3
                }
                break
            case 7: //less
                let pos1 = getValue(conf.pos1)
                let pos2 = getValue(conf.pos2)
                let pos3 = Position(mode: .immediate, index: memory[i+3])
                let value = pos1 < pos2 ? 1 : 0
                memory[pos3.index] = value
                i += 4
                break
            case 8: //eq
                let pos1 = getValue(conf.pos1)
                let pos2 = getValue(conf.pos2)
                let pos3 = Position(mode: .immediate, index: memory[i+3])
                let value = pos1 == pos2 ? 1 : 0
                memory[pos3.index] = value
                i += 4
                break
            default:
                print ("error - received OpCode \(opCode)")
            }
            instruction = memory[i]
            opCode = instruction % 100
        }
        halted = true
    }
    
    private func getValue(_ pos: Position) -> Int {
        let value: Int
        if (pos.mode == .immediate) {
            value = memory[pos.index]
        } else { //positional
            value = memory[memory[pos.index]]
        }
        return value
    }
}

public func readInputFile() -> [String] {
    let inputFile = URL(fileURLWithPath: "./input.txt")
    print("Path: \(inputFile.path)")
    do {
        let data = try String(contentsOfFile: inputFile.path, encoding: .utf8)
        return data.components(separatedBy: .newlines).filter { $0 != "" }
    } catch {
        print(error)
    }
    return [];
}

public typealias Phases = (a: Int, b: Int, c: Int, d: Int, e: Int)
public typealias Computers = (a: IntComputer, b: IntComputer, c: IntComputer, d: IntComputer, e: IntComputer)

public func runAmplifiers(phases: Phases, data: [Int], initValue: Int = 0, requestedComputers: Computers? = nil) -> Int {
    print("(a: \(phases.a), b: \(phases.b), c: \(phases.c), d: \(phases.d), e: \(phases.e)) -> ", terminator: "")
    let computers: Computers = requestedComputers ?? Computers(
        a: IntComputer(memory: data.map { $0 }),
        b: IntComputer(memory: data.map { $0 }),
        c: IntComputer(memory: data.map { $0 }),
        d: IntComputer(memory: data.map { $0 }),
        e: IntComputer(memory: data.map { $0 })
    )
    computers.a.execProgram(phase: phases.a, instruct: initValue)
    let a = computers.a.getCurrentValue()
    computers.b.execProgram(phase: phases.b, instruct: a)
    let b = computers.b.getCurrentValue()
    computers.c.execProgram(phase: phases.c, instruct: b)
    let c = computers.c.getCurrentValue()
    computers.d.execProgram(phase: phases.d, instruct: c)
    let d = computers.d.getCurrentValue()
    computers.e.execProgram(phase: phases.e, instruct: d)
    let e = computers.e.getCurrentValue()
    print("\(e)")
    return e
}

public func runAmplifiersWithFeedback(phases: Phases, data: [Int], initValue: Int = 0) -> Int {
    let computers: Computers = Computers(
        a: IntComputer(memory: data.map { $0 }),
        b: IntComputer(memory: data.map { $0 }),
        c: IntComputer(memory: data.map { $0 }),
        d: IntComputer(memory: data.map { $0 }),
        e: IntComputer(memory: data.map { $0 })
    )
    var initVal = initValue
    while (computers.e.isHalted() == false) {
        initVal = runAmplifiers(phases: phases, data: data, initValue: initVal, requestedComputers: computers)
    }
    return initVal
}

public func findBestPhasesOutput(input: [Int], feedback: Bool) -> Int {
    let from = feedback ? 5 : 0
    let to = feedback ? 9 : 4
    var best: Int = 0
    for a in from...to {
        for b in from...to {
            if (b == a) { continue }
            for c in from...to {
                if (c == b || c == a) { continue }
                for d in from...to {
                    if (d == c || d == b || d == a) { continue }
                    for e in from...to {
                        if (e == d || e == c || e == b || e == a) { continue }
                        let phases = Phases(a: a, b: b, c: c, d: d, e: e)
                        var value: Int?
                        if (feedback) {
                            value = runAmplifiersWithFeedback(phases: phases, data: input, initValue: 0)
                        } else {
                            value = runAmplifiers(phases: phases, data: input, initValue: 0)
                        }
                        if (best < value!) {
                            best = value!
                        }
                    }
                }
            }
        }
    }
    return best
}

public func calc(data: String) -> Void {
    let input: [Int] = data.components(separatedBy: ",").map {Int($0)!}
    let part1 = findBestPhasesOutput(input: input, feedback: false)
    let part2 = findBestPhasesOutput(input: input, feedback: true)
    print("Part1: Thrusters best value: \(part1)")
    print("Part2: Thrusters best value: \(part2)")
}

print("Hello, It's Advent of Code 2019 Task 07")
let startTS = Date().timeIntervalSince1970

let data: [String] = readInputFile()
calc(data: data.first!)

let finishTS = Date().timeIntervalSince1970

print("Time: \(finishTS - startTS) seconds")
