//
//  main.swift
//  AoC2019Task05
//
//  Created by Grzegorz Lieske on 01/12/2019.
//  Copyright © 2019 Grzegorz Lieske. All rights reserved.
//

import Foundation

extension StringProtocol {
    subscript(characterIndex: Int) -> String {
        return String(self[index(startIndex, offsetBy: characterIndex)])
    }
}

public class Position {
    public var mode: Mode
    public var index: Int
    init(mode: Mode, index: Int) {
        self.mode = mode
        self.index = index
    }
}

public class Config {
    public var pos1: Position
    public var pos2: Position
    public var pos3: Position
    public var opCode: Int
    init(_ opcode: Int, _ poss: [Position]) {
        self.opCode = opcode
        self.pos1 = poss[0]
        self.pos2 = poss[1]
        self.pos3 = poss[2]
    }
}

public enum Mode {
    case positional
    case immediate
}

public func getValue(data: [Int], pos: Position) -> Int {
    let value: Int
    if (pos.mode == .immediate) {
        value = data[pos.index]
    } else { //positional
        value = data[data[pos.index]]
    }
    return value
}

public func readInputFile() -> [String] {
    let inputFile = URL(fileURLWithPath: "./input.txt")
    print("Path: \(inputFile.path)")
    do {
        let data = try String(contentsOfFile: inputFile.path, encoding: .utf8)
        return data.components(separatedBy: .newlines).filter { $0 != "" }
        
    } catch {
        print(error)
    }
    return [];
}

public func getConfig(code: Int, index: Int) -> Config {
    var str = String(code)
    while str.count < 5 {
        str = "0\(str)"
    }
    return Config(
        code,
        str.dropLast().dropLast()
            .reversed()
            .enumerated()
            .map{ (i, elem) in return Position(mode: (Int(String(elem)) == 0 ? .positional : .immediate), index: index + i + 1) }
    )
}

public func execProgram(data: [Int]) -> Void {
    var input = data;
    var i = 0
    while i < input.count {
        let instruction = input[i]
        let opCode = instruction % 100
        if (opCode == 99) {
            break;
        }
        let conf = getConfig(
            code: instruction,
            index: i
        )
        switch (opCode) {
        case 1: // add
            let pos1 = getValue(data: input, pos: conf.pos1)
            let pos2 = getValue(data: input, pos: conf.pos2)
            let pos3 = input[i+3]
            input[pos3] = pos1 + pos2
            i += 4
            break
        case 2: // multiply
            let pos1 = getValue(data: input, pos: conf.pos1)
            let pos2 = getValue(data: input, pos: conf.pos2)
            let pos3 = input[i+3]
            input[pos3] = pos1 * pos2
            i += 4
            break
        case 3: // get input
            let pos1 = input[i+1]
            print("Provide input: ", terminator: "")
            let value = readLine()
            input[pos1] = Int(value!)!
            i += 2
            break
        case 4: // output
            let pos1 = getValue(data: input, pos: conf.pos1)
            print(pos1)
            i += 2
            break
        case 5: //jump true
            let value = getValue(data: input, pos: conf.pos1)
            let pos = getValue(data: input, pos: conf.pos2)
            if (value != 0) {
                i = pos
                //do not increment i
            } else {
                i += 3
            }
            break
        case 6: //jump false
            let value = getValue(data: input, pos: conf.pos1)
            let pos = getValue(data: input, pos: conf.pos2)
            if (value == 0) {
                i = pos
                //do not increment i
            } else {
                i += 3
            }
            break
        case 7: //less
            let pos1 = getValue(data: input, pos: conf.pos1)
            let pos2 = getValue(data: input, pos: conf.pos2)
            let pos3 = input[i+3]
            let value = pos1 < pos2 ? 1 : 0
            input[pos3] = value
            i += 4
            break
        case 8: //eq
            let pos1 = getValue(data: input, pos: conf.pos1)
            let pos2 = getValue(data: input, pos: conf.pos2)
            let pos3 = input[i+3]
            let value = pos1 == pos2 ? 1 : 0
            input[pos3] = value
            i += 4
            break
        default:
            print ("error")
        }
    }
}

public func calc(data: String) -> Void {
    let input: [Int] = data.components(separatedBy: ",").map {Int($0)!}
    return execProgram(data: input)
}

print("Hello, It's Advent of Code 2019 Task 05")
let startTS = Date().timeIntervalSince1970

let data: [String] = readInputFile()
calc(data: data.first!)

let finishTS = Date().timeIntervalSince1970

print("Time: \(finishTS - startTS) seconds")
