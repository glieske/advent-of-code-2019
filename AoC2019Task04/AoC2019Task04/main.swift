//
//  main.swift
//  AoC2019Task04
//
//  Created by Grzegorz Lieske on 01/12/2019.
//  Copyright © 2019 Grzegorz Lieske. All rights reserved.
//

import Foundation

public func isMatchingPart1(num: Int) -> Bool {
    let str = String(num)
    let nextDigitHigherIndices = str.indices.dropLast()
        .filter { Int(String(str[$0]))! > Int(String(str[str.index(after: $0)]))!}
    if nextDigitHigherIndices.count > 0 {
        return false // at least 1 times next digit is higher
    }
    let crossReference = Dictionary(grouping: str, by: { $0 }) // grouping string by digits (chars)
    if (crossReference.filter { $1.count > 1 }.count == 0) {
        return false // no doubles/triples etc.
    }
    return true
}

public func isMatchingP2(num: Int) -> Bool {
    let str = String(num)
    let crossReference = Dictionary(grouping: str, by: { $0 })
    let duplicates = crossReference
        .filter { $1.count == 2 } // has to have at least one double
    if duplicates.count == 0 {
        return false
    }
    return true
}

public func calculate(from: Int, to: Int) -> (Int, Int) {
    var counterP1 = 0
    var counterP2 = 0
    for i in from...to {
        if (isMatchingPart1(num: i)) {
            counterP1 += 1
            if (isMatchingP2(num: i)) {
                counterP2 += 1
            }
        }
    }
    return (counterP1, counterP2)
}

public func calculate(num: Int) -> (Int, Int) {
    return calculate(from: num, to: num)
}

print("Hello, It's Advent of Code 2019 Task 04")
let startTS = Date().timeIntervalSince1970

let from: Int = 278384
let to: Int = 824795
let result: (Int, Int) = calculate(from: from, to: to)

let finishTS = Date().timeIntervalSince1970

print("Part1: \(result.0)")
print("Part2: \(result.1)")
print("Time: \(finishTS - startTS) seconds")
