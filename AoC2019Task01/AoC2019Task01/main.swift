//
//  main.swift
//  AoC2019Task01
//
//  Created by Grzegorz Lieske on 01/12/2019.
//  Copyright © 2019 Grzegorz Lieske. All rights reserved.
//

import Foundation

let fm = FileManager()

public func getAmountByMass(mass: Int) -> Int {
    return Int(floor(Double(mass) / 3) - 2);
}

public func getFuelOfFuel(fuel: Int) -> Int {
    let additionalFuel = getAmountByMass(mass: fuel)
    if (additionalFuel <= 0) {
        return fuel;
    } else {
        return fuel + getFuelOfFuel(fuel: additionalFuel)
    }
}

public func getInputFilePath() -> URL {
    while (true) {
        print("Provide path of input file: ")
        if let path = readLine() {
            let url = URL(fileURLWithPath: path)
            if (fm.fileExists(atPath: url.path)) {
                return url
            } else {
                print("File not found")
            }
        }
    }
}

public func readInputFile(inputFile: URL) -> [String] {
    print("Path: \(inputFile.absoluteString)")
    do {
        let data = try String(contentsOfFile: inputFile.path, encoding: .utf8)
        return data.components(separatedBy: .newlines)
    } catch {
        print(error)
    }
    return [];
}

public func calculate(data: [String]) -> (Int, Int) {
    var fuelNeeded = 0;
    var fuelOfFuel = 0;
    for moduleMass: String in data {
        let fuel = getAmountByMass(mass: Int(moduleMass) ?? 0)
        let additional = getFuelOfFuel(fuel: fuel)
        print("P01: Mass: \(moduleMass) : fuel -> \(fuel) | additional: \(additional)")
        fuelNeeded += fuel
        fuelOfFuel += additional
    }
    return (fuelNeeded: fuelNeeded, fuelOfFuel: fuelOfFuel)
}

print("Hello, It's Advent of Code 2019 Task 01")

let inputFile: URL = getInputFilePath()
let data: [String] = readInputFile(inputFile: inputFile);
let result = calculate(data: data)


print("\n\nResult for Part One: \(result.0)");
print("Result for Part Two: [ Only Additional: \(result.1), Sum: \(result.0 + result.1)")





