//
//  main.swift
//  AoC2019Task08
//
//  Created by Grzegorz Lieske on 01/12/2019.
//  Copyright © 2019 Grzegorz Lieske. All rights reserved.
//

import Foundation

extension StringProtocol {
    subscript(characterIndex: Int) -> String {
        return String(self[index(startIndex, offsetBy: characterIndex)])
    }
}

public func readInputFile() -> [String] {
    let inputFile = URL(fileURLWithPath: "./input.txt")
    print("Path: \(inputFile.path)")
    do {
        let data = try String(contentsOfFile: inputFile.path, encoding: .utf8)
        return data.components(separatedBy: .newlines).filter { $0 != "" }
    } catch {
        print(error)
    }
    return [];
}

public func calc(data: String) -> Void {
    let input: [Int] = data.map { Int(String($0))! }
    let dims = (width: 25, height: 6)
    let size = dims.width * dims.height
    var part1Result = -1
    var fewerZeros = size
    var from = 0
    var layers: [[Int]] = []
    for to in stride(from: size - 1, to: input.count, by: size) {
        let layer = input[from...to]
        layers.append(Array(layer))
        let zeros = layer.filter { $0 == 0 }
        let zerosCount = zeros.count
        if (zerosCount < fewerZeros) {
            let ones = layer.filter { $0 == 1 }
            let twos = layer.filter { $0 == 2 }
            part1Result = ones.count * twos.count
            fewerZeros = zerosCount
        }
        from = to + 1
    }
    
    print("Part 1: \(part1Result)")
    print("Part 2:")
    var image = [Int](repeating: 2, count: size)
    for layer in layers.reversed() {
        for i in 0...layer.count - 1 {
            if (layer[i] != 2) {
                image[i] = layer[i]
            }
        }
    }
    from = 0
    for to in stride(from: dims.width - 1, to: image.count, by: dims.width) {
        for i in from...to {
            print("\(image[i] == 0 ? " " : "W")", terminator: "") // empty because the console is already black :)
        }
        print("")
        from = to + 1
    }
}

print("Hello, It's Advent of Code 2019 Task 07")
let startTS = Date().timeIntervalSince1970

let data: [String] = readInputFile()
calc(data: data.first!)

let finishTS = Date().timeIntervalSince1970

print("Time: \(finishTS - startTS) seconds")
